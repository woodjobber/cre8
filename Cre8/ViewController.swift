//
//  ViewController.swift
//  Cre8
//
//  Created by chengbin on 2021/8/16.
//

import UIKit
import CoreML
extension UILabel {
    @IBInspectable
    var rotation: Int {
        get {
            return 0
        } set {
            let radians = CGFloat(CGFloat(Double.pi) * CGFloat(newValue) / CGFloat(180.0))
            self.transform = CGAffineTransform(rotationAngle: radians)
        }
    }
}
class ViewController: UIViewController {
    var imageView: UIImageView = UIImageView()
    let model: DeepLabV3? = nil
    let context = CIContext(options: nil)
    var fmodel: Flowers!
    private let descLabel = UILabel(frame:.zero)
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        descLabel.frame = CGRect(x: 10, y: 100, width: view.frame.width - 10, height: 40)
        
        let image = #imageLiteral(resourceName: "globthistle")
//        imageView.image = removeBackground(image: image)
        imageView.frame = CGRect(x: view.frame.width / 2 - 227 / 2, y: 227, width:227, height: 227)
//        imageView.contentMode = .scaleAspectFit
        view.addSubview(imageView)
//        imageView.backgroundColor = .clear
        view.addSubview(descLabel)
        imageView.image = image
        descLabel.textColor = .blue
        descLabel.font = .systemFont(ofSize: 20)
        fmodel = getFlowerModel()
        let prediction = try? fmodel.prediction(data: getPixelBuffer(image: image)!)
        descLabel.text = "I think this is a \(String(describing: prediction?.classLabel ?? "未知"))."
    }
    
    
    private func getFlowerModel() -> Flowers? {
        do {
            let config = MLModelConfiguration()
            return try Flowers(configuration: config)
        } catch {
            print("Error loading model: \(error)")
            return nil
        }
    }
    
    
    
    private func getDeepLabV3Model() -> DeepLabV3? {
        do {
            let config = MLModelConfiguration()
            return try DeepLabV3(configuration: config)
        } catch {
            print("Error loading model: \(error)")
            return nil
        }
    }
    
    
    private func getPixelBuffer(image: UIImage) -> CVPixelBuffer? {
        let resizedImage = image.resized(to: CGSize(width: 227, height: 227),scale: image.scale)
        if let pixelBuffer = resizedImage.pixelBuffer(width: Int(resizedImage.size.width), height: Int(resizedImage.size.height)){
            return pixelBuffer
        }
        return nil
    }
    
     private func removeBackground(image:UIImage) -> UIImage?{
        let model = getDeepLabV3Model()!
        let resizedImage = image.resized(to: CGSize(width: 513, height: 513),scale: image.scale)
            if let pixelBuffer = resizedImage.pixelBuffer(width: Int(resizedImage.size.width), height: Int(resizedImage.size.height)){
        
                if let outputImage = (try? model.prediction(image: pixelBuffer))?.semanticPredictions.image(min: 0, max: 1, axes: (0,0,1)), let outputCIImage = CIImage(image:outputImage){
                    if let maskImage = removeWhitePixels(image:outputCIImage), let resizedCIImage = CIImage(image: resizedImage), let compositedImage = composite(image: resizedCIImage, mask: maskImage){
                        return UIImage(ciImage: compositedImage).resized(to: CGSize(width: image.size.width, height: image.size.height))
                    }
                }
            }
            return nil
        }
        
        private func removeWhitePixels(image:CIImage) -> CIImage?{
            let chromaCIFilter = chromaKeyFilter()
            chromaCIFilter?.setValue(image, forKey: kCIInputImageKey)
            return chromaCIFilter?.outputImage
        }
        
        private func composite(image:CIImage,mask:CIImage) -> CIImage?{
            return CIFilter(name:"CISourceOutCompositing",parameters:
                [kCIInputImageKey: image,kCIInputBackgroundImageKey: mask])?.outputImage
        }
        
        // modified from https://developer.apple.com/documentation/coreimage/applying_a_chroma_key_effect
        private func chromaKeyFilter() -> CIFilter? {
            let size = 64
            var cubeRGB = [Float]()
            
            for z in 0 ..< size {
                let blue = CGFloat(z) / CGFloat(size-1)
                for y in 0 ..< size {
                    let green = CGFloat(y) / CGFloat(size-1)
                    for x in 0 ..< size {
                        let red = CGFloat(x) / CGFloat(size-1)
                        let brightness = getBrightness(red: red, green: green, blue: blue)
                        let alpha: CGFloat = brightness == 1 ? 0 : 1
                        cubeRGB.append(Float(red * alpha))
                        cubeRGB.append(Float(green * alpha))
                        cubeRGB.append(Float(blue * alpha))
                        cubeRGB.append(Float(alpha))
                    }
                }
            }
            
            var data = Data()
            cubeRGB.withUnsafeBufferPointer { ptr in data = Data(buffer: ptr) }
            
            let colorCubeFilter = CIFilter(name: "CIColorCube", parameters: ["inputCubeDimension": size, "inputCubeData": data])
            return colorCubeFilter
        }
        
        // modified from https://developer.apple.com/documentation/coreimage/applying_a_chroma_key_effect
        private func getBrightness(red: CGFloat, green: CGFloat, blue: CGFloat) -> CGFloat {
            let color = UIColor(red: red, green: green, blue: blue, alpha: 1)
            var brightness: CGFloat = 0
            color.getHue(nil, saturation: nil, brightness: &brightness, alpha: nil)
            return brightness
        }
}



@IBDesignable
class EdgeInsetLabel: UILabel {
    var textInsets = UIEdgeInsets.zero {
        didSet { invalidateIntrinsicContentSize() }
    }

    override func textRect(forBounds bounds: CGRect, limitedToNumberOfLines numberOfLines: Int) -> CGRect {
        let insetRect = bounds.inset(by: textInsets)
        let textRect = super.textRect(forBounds: insetRect, limitedToNumberOfLines: numberOfLines)
        let invertedInsets = UIEdgeInsets(top: -textInsets.top,
                left: -textInsets.left,
                bottom: -textInsets.bottom,
                right: -textInsets.right)
        return textRect.inset(by: invertedInsets)
    }

    override func drawText(in rect: CGRect) {
        super.drawText(in: rect.inset(by: textInsets))
    }
}

extension EdgeInsetLabel {
    @IBInspectable
    var leftTextInset: CGFloat {
        set { textInsets.left = newValue }
        get { return textInsets.left }
    }

    @IBInspectable
    var rightTextInset: CGFloat {
        set { textInsets.right = newValue }
        get { return textInsets.right }
    }

    @IBInspectable
    var topTextInset: CGFloat {
        set { textInsets.top = newValue }
        get { return textInsets.top }
    }

    @IBInspectable
    var bottomTextInset: CGFloat {
        set { textInsets.bottom = newValue }
        get { return textInsets.bottom }
    }
}
